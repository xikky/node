//console.log( 'Starting notes.js' );

const fs = require( 'fs' );

var fetchNotes = () => {

  try {

    var notesString = fs.readFileSync( 'notes-data.json' );
    return JSON.parse( notesString );
  } catch ( e ) {

    return [];
  }
};

var saveNotes = ( notes ) => {

  fs.writeFileSync( 'notes-data.json', JSON.stringify( notes ) );
};

var addNote = ( title, body ) => {

  var notes = fetchNotes();

  var note = {
    title,
    body
  };

  var duplicateNotes = notes.filter( ( note ) => note.title === title );
  // var duplicateNotes = notes.filter( ( note ) => {
  //
  //   inserts note in var only if true
  //   return note.title === title;
  // } );

  if ( duplicateNotes.length === 0 ) {

    notes.push( note );

    saveNotes( notes );

    return note;
  }

  //console.log( 'Adding note', title, body );
  //return 'New note';
};

var getAll = () => {

  return fetchNotes();
};

var getNote = ( title ) => {

  var notes = fetchNotes();

  var filteredNote = notes.filter( ( note ) => note.title === title );

  if ( filteredNote.length === 1 )
    return filteredNote[ 0 ];

  //console.log( 'Getting note: ', title );
};

var removeNote = ( title ) => {

  var notes = fetchNotes();

  var filteredNotes = notes.filter( ( note ) => note.title !== title );

  saveNotes( filteredNotes );

  if ( notes.length !== filteredNotes.length )
    return true;
  else
    return false;
};

var logNote = ( note ) => {

  console.log( '----' );
  console.log( `Title: ${note.title}` );
  console.log( `Body: ${note.body}` );
}

module.exports = {
  //addNote: addNote
  addNote,
  getAll,
  getNote,
  removeNote,
  logNote
};