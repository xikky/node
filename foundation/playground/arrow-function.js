var square = x => x * x;

//var square = ( x ) => x * x;

// var square = ( x ) => {
//
//   var result = x * x;
//   return result;
// };

console.log( square( 9 ) );

var user = {
  name  : "Andrew",
  sayHi : () => {
    //this will print global arguments
    //console.log( arguments );
    console.log( `Hi. I'm ${this.name}` );
  },
  sayHiAlt () {
    console.log( arguments );
    console.log( `Hi. I'm ${this.name}` )
  }
};

user.sayHi();
user.sayHiAlt( 1, 2, 3 );