//console.log( 'Starting app.js' );

const fs = require( 'fs' );
// const os = require( 'os' );

const _ = require( 'lodash' );
/*npm install yargs@4.7.1*/
const yargs = require( 'yargs' );

const notes = require( './notes.js' );

const titleOptions = {
  describe : 'Title of note',
  demand   : true,
  alias    : 't'
};
const bodyOptions = {
  describe : 'Body of note',
  demand   : true,
  alias    : 'b'
};

const argv = yargs
  .command( 'add', 'Add a new note', {
    title : titleOptions,
    body  : bodyOptions
  } )
  .command( 'list', 'List all notes' )
  .command( 'read', 'Read a note', {
    title : titleOptions
  } )
  .command( 'remove', 'Remove a note', {
    title : titleOptions
  } )
  .help()
  .argv;
//const argv = yargs.argv;

/*node app.js list*/
var command = argv._[ 0 ];
//var command = process.argv[ 2 ];
//console.log( 'Command: ', command );
// console.log( 'Process: ', process.argv );
/*node app.js add --title="Secret from Daniel"*/
//console.log( 'Yargs: ', argv );

if ( command === 'add' ) {

  var note = notes.addNote( argv.title, argv.body );

  if ( note ) {
    console.log( 'Note created.' );
    notes.logNote( note );
  }
  else
    console.log( 'Note already in use.' );
}
else if ( command === 'list' ) {

  var allNotes = notes.getAll();

  console.log( `Printing ${allNotes.length} note(s).` );

  allNotes.forEach( ( note ) => notes.logNote( note ) );
}
else if ( command === 'read' ) {

  var note = notes.getNote( argv.title );

  if ( note ) {

    console.log( 'Note found.' );
    notes.logNote( note );
  }
  else
    console.log( 'Note note found.' );
}
else if ( command === 'remove' ) {

  var noteRemoved = notes.removeNote( argv.title );
  var message = noteRemoved ? 'Note was removed.' : 'Not was not found.';
  console.log( message );
}
else
  console.log( 'Not recognized.' );

// var filteredArray = _.uniq( [ 'DanielS', 1, 'DanielS', 1, 2, 3, 4 ] );
// console.log( filteredArray );

// console.log( _.isString( true ) );
// console.log( _.isString( 'Daniel' ) );

// console.log( 'Result: ', notes.add( 5, 6 ) );

// var user = os.userInfo();
//
// fs.appendFile( 'greetings.txt', `Hello ${user.username}! You are ${notes.age}.`, function ( err ) {
//
//   if ( err ) {
//     console.log( 'Unable to write to file' );
//   }
// } );