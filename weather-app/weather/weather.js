const request = require( 'request' );

var getTemperature = ( latitude, longitude, callback ) => {

  request( {
             url  : `https://api.darksky.net/forecast/f2f07c15ccdd074c136a09fb86e512b7/${latitude},${longitude}`,
             json : true
           }, ( error, response, body ) => {
    if ( error )
      callback( 'Unable to connect to Forecast server.' );
    else if ( response.statusCode === 400 )
      callback( 'Unable to fetch weather.' );
    else if ( response.statusCode === 200 ) {
      callback( undefined, {
        temperature         : body.currently.temperature,
        apparentTemperature : body.currently.apparentTemperature
      } );
    }
  } );
};

module.exports = {
  getTemperature
};