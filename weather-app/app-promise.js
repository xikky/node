const yargs = require( 'yargs' );
const axios = require( 'axios' );

const argv = yargs.options( {
                              a : {
                                demand   : true,
                                alias    : 'address',
                                describe : 'Address to fetch weather for',
                                string   : true
                              }
                            } )
  .help()
  .alias( 'help', 'h' )
  .argv;

var encodedAddress = encodeURIComponent( argv.address );
var geocodeURL = `http://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

axios.get( geocodeURL ).then( ( response ) => {

  if ( response.data.status === 'ZERO_RESULTS' )
    throw new Error( 'Unable to find that address.' );

  var weatherURL = `https://api.darksky.net/forecast/f2f07c15ccdd074c136a09fb86e512b7/${response.data.results[ 0 ].geometry.location.lat},${response.data.results[ 0 ].geometry.location.lng}`;
  console.log( response.data.results[ 0 ].formatted_address );

  return axios.get( weatherURL );
} )
  .then( ( response ) => {

    console.log( `It's currently ${response.data.currently.temperature}. It feels like ${response.data.currently.apparentTemperature}` );
  } ).catch( ( error ) => {

  if ( error.code === 'ENOTFOUND' )
    console.log( 'Unable to connect to API servers.' );
  else {
    console.log( error.message );
  }
} );