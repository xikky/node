const yargs = require( 'yargs' );

const geocode = require( './geocode/geocode' );
const weather = require( './weather/weather' );

const argv = yargs.options( {
                              a : {
                                demand   : true,
                                alias    : 'address',
                                describe : 'Address to fetch weather for',
                                string   : true
                              }
                            } )
  .help()
  .alias( 'help', 'h' )
  .argv;

geocode.geocodeAddress( argv.address, ( error, results ) => {

  if ( error )
    console.log( error );
  else {
    weather.getTemperature( results.longitude, results.latitude, ( error, results ) => {

      if ( error )
        console.log( error );
      else
        console.log( JSON.stringify( `Temperature is ${results.temperature} but the apparant temperature is ${results.apparentTemperature}`, undefined, 2 ) );
    } );
  }
} );

