const request = require( 'request' );

var geocodeAddress = ( address ) => {

  return new Promise( ( resolve, reject ) => {

    request( {
               url  : `http://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent( address )}`,
               json : true
             }, ( error, response, body ) => {

      if ( error )
        reject( 'Unable to connect to Google servers.' );
      else if ( body.status === "ZERO_RESULTS" )
        reject( 'Unable to find address.' );
      else if ( body.status === "OK" ) {

        resolve( {
                   latitude  : body.results[ 0 ].geometry.location.lat,
                   longitude : body.results[ 0 ].geometry.location.lng
                 } );
      }
    } );
  } );
};

geocodeAddress( '19146' ).then( ( location ) => {

  return new Promise( ( resolve, reject ) => {

    request( {
               url  : `https://api.darksky.net/forecast/f2f07c15ccdd074c136a09fb86e512b7/${location.latitude},${location.longitude}`,
               json : true
             }, ( error, response, body ) => {
      if ( error )
        reject( 'Unable to connect to Forecast server.' );
      else if ( response.statusCode === 400 )
        reject( 'Unable to fetch weather.' );
      else if ( response.statusCode === 200 )
        console.log( JSON.stringify( `Temperature is ${body.currently.temperature} but the apparant temperature is ${body.currently.apparentTemperature}`, undefined, 2 ) );
    } );
  } );
} ).catch( ( error ) => {
  console.log( error );
} );