var asyncAdd = ( a, b ) => {

  return new Promise( ( resolve, reject ) => {
    setTimeout( () => {

      if ( typeof a === 'number' && typeof b === 'number' )
        resolve( a + b );
      else
        reject( 'Values are not numbers.' );
    }, 1500 );
  } );
};

asyncAdd( 5, 3 ).then( ( result ) => {

  console.log( 'Result: ', result );

  return asyncAdd( result, 5 );
} ).then( ( result ) => {

  console.log( 'Second Result: ', result );
} ).catch( ( error ) => {

  console.log( error );
} );

// var somePromise = new Promise( ( resolve, reject ) => {
//
//   setTimeout( () => {
//     resolve( 'Hey. It worked.' );
//   }, 2500 );
//
//   reject( 'Unable to fulfill promise.' );
// } );
//
// somePromise.then( ( message ) => {
//
//   console.log( 'Success: ', message );
// }, ( error ) => {
//
//   console.log( 'Error: ', error );
// } );