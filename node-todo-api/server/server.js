const express = require( 'express' );
const bodyParser = require( 'body-parser' );

var { mongoose } = require( './db/mongoose' );
var { Todo } = require( './models/todo' );
var { User } = require( './models/user' );

var app = express();

app.use( bodyParser.json() );

app.post( '/todos', ( request, response ) => {

  var todo = new Todo( {
    text : request.body.text,
  } );

  todo.save().then( ( doc ) => {
    response.send( doc );
  }, ( error ) => {
    response.status( 400 ).send( error );
  } );
} );

app.listen( 3000, () => {
  console.log( 'Started on port 3000' );
} );

// var Todo = mongoose.model( 'Todo', {
//   text        : {
//     type      : String,
//     required  : true,
//     minlength : 1,
//     trim      : true
//   },
//   completed   : {
//     type    : Boolean,
//     default : false
//   },
//   completedAt : {
//     type    : Number,
//     default : null
//   }
// } );

// var newTodo = new Todo( {
//   text : 'Cook dinner'
// } );
//
// newTodo.save().then( ( doc ) => { console.log( 'Saved todo', doc ) },
//                      ( e ) => { console.log( 'Unable to save todo.' ) } );

// var otherTodo = new Todo( {
//   text        : 'Feed the cat',
//   completed   : true,
//   completedAt : 123
// } );
//
// otherTodo.save().then( ( doc )=> {
//   console.log( JSON.stringify( doc, undefined, 2 ) );
// }, ( e ) => {
//   console.log( 'Unable to save', e );
// } );

// var User = mongoose.model( 'User', {
//
//   email : {
//     type      : String,
//     require   : true,
//     trim      : true,
//     minlength : 1
//   }
// } );
//
// var myUser = new User( {
//   email : 'koxxa@hotmail.com'
// } );
//
// myUser.save().then( ( doc ) => {
//   console.log( JSON.stringify( doc, undefined, 2 ) );
// }, ( e ) => {
//   console.log( 'Unable to create user', e );
// } )