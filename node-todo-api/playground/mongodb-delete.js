const { MongoClient, ObjectID } = require( 'mongodb' );

MongoClient.connect( 'mongodb://localhost:27017/TodoApp', ( error, db ) => {

  if ( error )
    return console.log( 'Unable to connect to MongoDB server.' );

  console.log( 'Connected to MongoDB server' );

  /* Delete many */
  // db.collection( 'Todos' ).deleteMany( { text : "Eat lunch" } ).then( ( result ) => {
  //
  //   console.log( result );
  // } );

  /* Delete one */
  // db.collection( 'Todos' ).deleteOne( { text : "Eat lunch" } ).then( ( result ) => {
  //
  //   console.log( result );
  // } );

  /* Find One and Delete */
  // db.collection( 'Todos' ).findOneAndDelete( { complete : false } ).then( ( result )=> {
  //
  //   console.log( result );
  // } );

  db.collection( 'Users' ).deleteMany( { name : "Daniel" } );

  db.collection( 'Users' ).findOneAndDelete( { _id : new ObjectID( "59aae7a422c80b08289b2aca" ) } ).then( ( result )=> {

    console.log( JSON.stringify( result, undefined, 2 ) );
  } );

  db.close();
} );