//const MongoClient = require( 'mongodb' ).MongoClient;
/*Using Object destructuring*/
const { MongoClient, ObjectID } = require( 'mongodb' );

/*generating an object id which can be handled by mongodb automatically when inserting a document*/
// var obj = new ObjectID();
// console.log( obj );

/*Object destructuring*/
// var user = { name : 'Daniel', age : 27 };
// var { name } = user;
// console.log( name );

MongoClient.connect( 'mongodb://localhost:27017/ToDoApp', ( error, db ) => {

  if ( error )
    return console.log( 'Unable to connect to MongoDB server.' );

  console.log( 'Connected to MongoDB server' );

  // db.collection( 'Todos' ).insertOne( {
  //                                       text      : 'Something to do',
  //                                       completed : false
  //                                     }, ( error, result ) => {
  //
  //   if ( error )
  //     return console.log( 'Unable to insert todo', error );
  //
  //   console.log( JSON.stringify( result.ops, undefined, 2 ) );
  // } );

  // db.collection( 'Users' ).insertOne( {
  //                                       name     : 'Daniel',
  //                                       age      : 27,
  //                                       location : 'Malta'
  //                                     }, ( error, result ) => {
  //
  //   if ( error )
  //     return console.log( 'Unable to insert user', error );
  //
  //   console.log( result.ops[ 0 ]._id.getTimestamp() );
  // } );

  db.close();
} );