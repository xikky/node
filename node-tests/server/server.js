const express = require( 'express' );

var app = express();

app.get( '/', ( request, respond ) => {

  // respond.status( 404 ).send( 'Hello world!' );
  respond.status( 404 ).send( {
                                error : 'Page not found.',
                                name  : 'Todo App v1.0'
                              } );
} );

app.get( '/users', ( request, respond ) => {

  respond.send( [
                  { name : 'Daniel', age : '27' },
                  { name : 'Jean', age : '22' }
                ]
  );
} );

app.listen( 3000 );

module.exports.app = app;