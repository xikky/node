const request = require( 'supertest' );
const expect = require( 'expect' );

var app = require( './server' ).app;

describe( 'Server', () => {

  describe( 'GET /', () => {

    it( 'should return hello world response', ( done ) => {

      request( app )
        .get( '/' )
        .expect( 404 )
        .expect( ( res ) => {
          /*using expect package*/
          expect( res.body ).toInclude( {
                                          error : 'Page not found.',
                                          name  : 'Todo App v1.0'
                                        } );
        } )
        // .expect( {
        //            error : 'Page not found.'
        //          } )
        // .expect( 'Hello world!' )
        .end( done );
    } );
  } );

  describe( 'GET /users', () => {

    it( 'should return a user object', ( done ) => {

      request( app )
        .get( '/users' )
        .expect( 200 )
        .expect( ( res ) => {

          expect( res.body ).toInclude( { name : 'Daniel', age : '27' } );
        } )
        .end( done )
    } );
  } );
} );