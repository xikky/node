/*nodemon --exec "npm test" :: to watch tests or any other commands*/
/*custom script name need to be run as follows: npm run test-watch*/

const expect = require( 'expect' );

const utils = require( './utils.js' );

describe( 'Utils', () => {

  describe( '#add', () => {

    it( 'should add two numbers', () => {

      var res = utils.add( 33, 11 );

      expect( res ).toBe( 44 ).toBeA( 'number' );

      // if ( res !== 44 )
      //   throw new Error( `Expected 44, but got ${res}.` );
    } );
  } );

  it( 'should async add two numbers', ( done ) => {

    utils.asyncAdd( 4, 3, ( sum ) => {

      expect( sum ).toBe( 7 ).toBeA( 'number' );

      done();
    } );
  } );

  it( 'should square a number', () => {

    var res = utils.square( 5 );

    expect( res ).toBe( 25 ).toBeA( 'number' );

    // if ( res !== 25 )
    //   throw new Error( `Expected 25, but got ${res}.` );
  } );

  it( 'should async square a number', ( done ) => {

    utils.asyncSquare( 3, ( sum ) => {

      expect( sum ).toBe( 9 ).toBeA( 'number' );

      done();
    } );
  } );

  it( 'should expect some values', () => {

    // expect( 12 ).toNotBe( 11 );
    /* For objects toBe needs to be replaced to toEqual / toNotEqual ( === against == )*/
    // expect( { name : 'Daniel' } ).toEqual( { name : 'Daniel' } );
    // expect( [ 2, 3, 4 ] ).toInclude( 2 );
    // expect( [ 2, 3, 4 ] ).toExclude( 5 );
    expect( {
              name     : 'Daniel',
              age      : 27,
              location : 'Malta'
            } ).toInclude( {
                             age : 27
                           } );
  } );

  it( 'should verify first and last names are set', () => {

    var user = {
      age      : 25,
      location : 'Malta'
    }

    user = utils.setName( user, 'Daniel Scicluna' );

    expect( user ).toInclude( {
                                firstName : 'Daniel',
                                lastName  : 'Scicluna'
                              } );
  } );
} );